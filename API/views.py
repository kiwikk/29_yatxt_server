from django.shortcuts import render
from django.http import HttpResponse
from json import dumps
from os import listdir


# Create your views here.


def get_all_books(request):
    return HttpResponse(' '.join(listdir('books')))


def get_all_pages_names(request):
    name = request.GET['name']
    pages = ''
    for file in listdir('books/' + name):
        if file != 'cover.jpg' and 'book-info.xml':
            pages = pages + ' ' + file
    return HttpResponse(pages)


def get_book_info(request):
    book_name = request.GET['name']
    return HttpResponse(open('books/' + book_name + '/book-info.xml', 'r').read())


def get_page(request):
    book_name = request.GET['name']
    book_page = request.GET['page']
    return HttpResponse(open('books/' + book_name + '/' + book_page + '.xml', 'rb').read().decode('utf-8'))


def get_cover(request):
    book_name = request.GET['name']
    book_image = open('books/' + book_name + '/' + '/cover.jpg', 'rb').read()
    return HttpResponse(book_image, content_type='image/jpg')


def download_file(request):
    name = request.GET['name']
    file = request.GET['file']
    f = open('books/' + name + '/' + file, 'rb').read()
    response = HttpResponse(f, content_type='application/force-download')
    response['Content-Disposition'] = 'inline; filename=' + file
    return response
