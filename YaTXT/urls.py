"""YaTXT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from API import views

# '' - returns all books' name in one str (split ' ')
# 'name' - returns book info (name)
# 'pages' - returns all pages' names in one str (split ' ') (name)
# 'page' - returns xml page in web version (name page)
# 'cover' - returns a cover of the book (name)
# 'download' - returns a file (name file) (but where..?)
# name is a book-name

urlpatterns = [
    path('', views.get_all_books),
    path('name', views.get_book_info),
    path('pages', views.get_all_pages_names),
    path('page', views.get_page),
    path('cover', views.get_cover),
    path('download', views.download_file)
]
